from django.contrib import admin
from .models import LikesModel, BookmarkModel

admin.site.register(LikesModel)
admin.site.register(BookmarkModel)
# Register your models here.
