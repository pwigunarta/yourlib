$(document).ready(function () {
    $('#searchBtn').on("click", function (event) {
        event.preventDefault();
        var search = $('#searchIn').val();
        var searchType = $('#searchType').val();
        $.ajax({
            url: '/searchResult/' + searchType + '/' + search,
            async: false,
            success: function (data) {
                let searchResult = '';
                console.log(data);
                if (data.Response == "True") {
                    searchResult = `<div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="${data.Poster}" alt="Card image cap">
                    <div class="card-body">
                      <h5 class="card-title">${data.Title}(${data.Year})</h5>
                      <p class="card-text">${data.Plot}</p>
                      <button type="button" class="btn btn-outline-success my-2 my-sm-0 btn-primary" id="likesBtn">Like</button>
                      <button type="button" class="btn btn-outline-success my-2 my-sm-0 btn-primary" id="bookmarkBtn">Bookmark</button>
                    </div>
                    </div>`;
                    
                }else{
                    searchResult = `<h1 class="justify-content-center text-center"> Tidak Ditemukan Data dengan Keyword tersebut </h1>`;
                }
                $('#searchBox').children('#results').html(searchResult);
                likeTrigger(data);
                likeToggle(data.imdbID, data.Title, data.Plot, data.Year, data.Poster);
                bookmarkTrigger(data);
                bookmarkToggle(data.imdbID, data.Title, data.Plot, data.Year, data.Poster);
            },
            error: function(){
                searchResult = `<h1 class="justify-content-center text-center"> Tidak Ditemukan Data dengan Keyword tersebut </h1>`;
                $('#searchBox').children('#results').html(searchResult);
            }
        })
    })


})

function likeTrigger(data) {
    if(data.isLiked == 'False'){
        $('#likesBtn').removeClass("active");
        $('#likesBtn').html("Like");
    }else{
        $('#likesBtn').addClass("active");
        $('#likesBtn').html("Unlike");
    }

    $('#likesBtn').on("click", function (event) {
        event.preventDefault();
        var id = $('#imdbid').html();
        var title = $('#title').html();
    })
}

function likeToggle(id, title, plot, year, poster) {
    $('#likesBtn').on("click", function (event) { 
            $.ajax({
                url: 'likes/' + id + '/' + title ,
                data: { plot: plot, year:year, poster :poster},
                success: function (data) {
                    if(data.isLiked == 'False'){
                        $('#likesBtn').removeClass("active");
                        $('#likesBtn').html("Like");
                    }else{
                        $('#likesBtn').addClass("active");
                        $('#likesBtn').html("Unlike");
                    }
                    
                }
            })
    })
}

function bookmarkTrigger(data) {
    if(data.isBookmarked == 'False'){
        $('#bookmarkBtn').removeClass("active");
        $('#bookmarkBtn').html("Bookmark");
    }else{
        $('#bookmarkBtn').addClass("active");
        $('#bookmarkBtn').html("Unbook");
    }

    $('#bookmarkBtn').on("click", function (event) {
        event.preventDefault();
        var id = $('#imdbid').html();
        var title = $('#title').html();
    })
}

function bookmarkToggle(id, title, plot, year, poster) {
    

    $('#bookmarkBtn').on("click", function (event) {
        
            $.ajax({
                url: 'bookmark/' + id + '/' + title,
                data: { plot: plot, year:year, poster :poster},
                success: function (data) {
                    if(data.isBookmarked == 'False'){
                        $('#bookmarkBtn').removeClass("active");
                        $('#bookmarkBtn').html("Bookmark");
                    }else{
                        $('#bookmarkBtn').addClass("active");
                        $('#bookmarkBtn').html("Unbook");
                    }
                    
                }
            })
    })
    
}


