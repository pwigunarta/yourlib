from django.shortcuts import render
import requests
from django.http import JsonResponse
import json
from .models import LikesModel, BookmarkModel
from django.shortcuts import redirect


def home(request):
    return render(request, 'main/home.html')

def likeViews (request, id, ttl):
    year = request.GET.get('year')
    plot = request.GET.get('plot')
    poster = request.GET.get('poster')
    objNow = LikesModel.objects.filter(movieId = id)
    if(objNow.exists()):
        objNow.delete()
    else:
        newObj = LikesModel.objects.create(movieId = id, title = ttl, year = year, plotStr = plot, poster = poster)
        newObj.save()
    
    updateObj = searchView(request, 1, id)

    return updateObj

def bookmarkViews (request, id, ttl):
    objNow = BookmarkModel.objects.filter(movieId = id)
    year = request.GET.get('year')
    plot = request.GET.get('plot')
    poster = request.GET.get('poster')
    if(objNow.exists()):
        objNow.delete()
    else:
        newObj = BookmarkModel.objects.create(movieId = id, title = ttl, year = year, plotStr = plot, poster = poster)
        newObj.save()
    
    updateObj = searchView(request, 1, id)

    return updateObj


def searchView(request, options, key):
    result=0
    objLiked=""
    objBookmarked=""
    if (options == 2):
        result = requests.get('http://www.omdbapi.com/?apikey=e1f98568&t=' + key).json()
        objLiked = LikesModel.objects.filter(movieId=result['imdbID'])
        objBookmarked = BookmarkModel.objects.filter(movieId=result['imdbID'])
    else:
        result = requests.get('http://www.omdbapi.com/?apikey=e1f98568&i=' + key).json()
        objLiked = LikesModel.objects.filter(movieId=key)
        objBookmarked = BookmarkModel.objects.filter(movieId=key)

    #Search Model dan jika ada

    if(objLiked.exists()):
        result ['isLiked'] = 'True'
    else:
        result ['isLiked'] = 'False'

    if(objBookmarked.exists()):
        result ['isBookmarked'] = 'True'
    else:
        result ['isBookmarked'] = 'False'
    
    return JsonResponse(result)

def search(request):
    return render(request, 'main/search.html')

def likeList(request):
    allLikes = LikesModel.objects.all()
    return render(request, 'main/likeList.html', {'allLikes': allLikes})

def bookmarkList(request):
    allBookmark = BookmarkModel.objects.all()
    return render(request, 'main/bookmarkList.html', {'allBookmark': allBookmark})    

def unlike(request, id):
    LikesModel.objects.filter(movieId=id).delete()
    return redirect('/likeList/')

def unbook(request, id):
    BookmarkModel.objects.filter(movieId=id).delete()
    return redirect('/bookmarkList/')