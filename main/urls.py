from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('searchResult/<int:options>/<str:key>', views.searchView, name="search"),
    path('likes/<str:id>/<str:ttl>/', views.likeViews, name="likes"),
    path('bookmark/<str:id>/<str:ttl>/', views.bookmarkViews, name="bookmark"),
    path('search/', views.search, name="search"),
    path('likeList/', views.likeList, name="likeList"),
    path('bookmarkList/', views.bookmarkList, name="bookmarkList"),
    path('unlike/<str:id>', views.unlike, name="unlike"),
    path('unbook/<str:id>', views.unbook, name="unbook"),
]
