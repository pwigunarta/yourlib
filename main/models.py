from django.db import models

# Create your models here.

class LikesModel(models.Model):
    movieId = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    year = models.CharField(max_length=255)
    plotStr = models.CharField(max_length=255, null=True)
    poster = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.title
    
    def get_unlike_url(self):
        return "/unlike/" + self.movieId

class BookmarkModel(models.Model):
    movieId = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    year = models.CharField(max_length=255)
    plotStr = models.CharField(max_length=255, null=True)
    poster = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.title

    def get_unbook_url(self):
        return "/unbook/" + self.movieId