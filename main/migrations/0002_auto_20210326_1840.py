# Generated by Django 3.1.2 on 2021-03-26 11:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookmarkmodel',
            name='plotStr',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='bookmarkmodel',
            name='poster',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='bookmarkmodel',
            name='year',
            field=models.CharField(default='No Plot Avaiable', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='likesmodel',
            name='plotStr',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='likesmodel',
            name='poster',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='likesmodel',
            name='year',
            field=models.CharField(default='N/A', max_length=255),
            preserve_default=False,
        ),
    ]
